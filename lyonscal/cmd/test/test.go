package main

import (
	_ "embed"
	"flag"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"os"
	"time"

	"gitlab.com/mirukakoro/mirukakoro.gitlab.io/lyonscal/api"
	"gitlab.com/mirukakoro/mirukakoro.gitlab.io/lyonscal/cmd"
)

func main_() error {
	var baseURL *url.URL
	var timeout time.Duration
	var err error
	{
		_, _ = fmt.Fprint(os.Stderr, cmd.StartupInfo)
		oldUsage := flag.Usage
		flag.Usage = func() {
			_, _ = fmt.Fprintf(os.Stderr, cmd.LicenseInfo)
			oldUsage()
		}
	}
	{
		var rawBaseURL string
		flag.StringVar(&rawBaseURL, "base-url", api.DefaultBaseURL.String(), "base URL of the API to use")
		flag.DurationVar(&timeout, "timeout", 1*time.Second, "timeout for API")
		flag.Parse()
		baseURL, err = url.Parse(rawBaseURL)
		if err != nil {
			return fmt.Errorf("timeout format: %w", err)
		}
	}
	client := api.NewClient(&http.Client{Timeout: timeout}, baseURL)
	start := time.Now()
	start.Add(-24 * time.Hour)
	end := time.Now().Add(7 * 24 * time.Hour)
	events, err := client.Events(api.EventsReq{
		Start: &start,
		End:   &end,
	})
	if err != nil {
		return err
	}
	log.Printf("%d event(s)", len(events))
	for i, event := range events {
		if event.Name == "Grade 9 Orientation" {
			log.Printf("%d: %s", i, event)
		}
	}
	return nil
}

func main() {
	err := main_()
	if err != nil {
		log.Fatal(err)
	}
}
