package main

import (
	"embed"
	_ "embed"
	"flag"
	"fmt"
	"gitlab.com/mirukakoro/mirukakoro.gitlab.io/lyonscal/api"
	"gitlab.com/mirukakoro/mirukakoro.gitlab.io/lyonscal/cmd"
	"io"
	"log"
	"net/http"
	"net/url"
	"os"
	"strings"
	"sync"
	"text/template"
	"time"
)

//go:embed tmpls/*.md
var tmpls embed.FS

func gen(tmpl *template.Template, name string, i int, event api.Event) (err error) {
	var file io.WriteCloser
	err = os.MkdirAll(fmt.Sprintf("events/%d", i), 0777)
	if err != nil {
		return err
	}
	file, err = os.Create(fmt.Sprintf("events/%d/index.%s.md", i, name))
	if err != nil {
		return err
	}
	defer func(file io.WriteCloser) {
		err2 := file.Close()
		if err2 != nil {
			err = err2
		}
	}(file)
	err = tmpl.ExecuteTemplate(file, name, event)
	if err != nil {
		return err
	}
	return
}

func main_() error {
	var baseURL *url.URL
	var timeout time.Duration
	var start *time.Time
	var duration time.Duration
	var names []string
	var err error
	{
		_, _ = fmt.Fprint(os.Stderr, cmd.StartupInfo)
		oldUsage := flag.Usage
		flag.Usage = func() {
			_, _ = fmt.Fprintf(os.Stderr, cmd.LicenseInfo)
			oldUsage()
		}
	}
	{
		var rawBaseURL string
		var rawStart string
		var rawNames string
		flag.StringVar(&rawBaseURL, "base-url", api.DefaultBaseURL.String(), "base URL of the API to use")
		flag.DurationVar(&timeout, "timeout", 1*time.Second, "timeout for API")
		flag.StringVar(&rawStart, "start", "", "start of time frame in RFC 3339 foramt")
		flag.DurationVar(&duration, "duration", 0, "duration of time frame")
		flag.StringVar(&rawNames, "names", "en-ca ja-jp", "names separated by a space")
		flag.Parse()
		names = strings.Split(rawNames, " ")
		baseURL, err = url.Parse(rawBaseURL)
		if err != nil {
			return fmt.Errorf("timeout format: %w", err)
		}
		if rawStart != "" {
			start2, err := time.Parse(time.RFC3339, rawStart)
			if err != nil {
				return fmt.Errorf("start format: %w", err)
			}
			start = &start2
		}
	}
	var resp api.EventsResp
	{
		client := api.NewClient(&http.Client{Timeout: timeout}, baseURL)
		var end *time.Time
		if start != nil {
			if duration != 0 {
				end2 := start.Add(duration)
				end = &end2
			}
		}
		resp, err = client.Events(api.EventsReq{
			Start: start,
			End:   end,
		})
		if err != nil {
			return fmt.Errorf("api: %w", err)
		}
	}
	{
		tmpl := template.New("root")
		_, err = tmpl.Parse("")
		if err != nil {
			return err
		}
		tmpl.Funcs(template.FuncMap{
			"now": func() string { return time.Now().Format(time.RFC3339) },
		})

		tmpl, err = tmpl.ParseFS(tmpls, "tmpls/*.md")
		if err != nil {
			return fmt.Errorf("parse tmpls: %w", err)
		}
		var wg sync.WaitGroup
		for i, event := range resp {
			for _, name := range names {
				wg.Add(1)
				go func(name string, i int, event api.Event) {
					err := gen(tmpl, name, i, event)
					if err != nil {
						log.Printf("%d/%s: error: %s", i, name, err)
					}
					defer log.Printf("%d/%s: done", i, name)
					defer wg.Done()
				}(name, i, event)
			}
		}
		wg.Wait()
	}
	return nil
}

func main() {
	err := main_()
	if err != nil {
		log.Fatal(err)
	}
}
