{{define "en-ca" -}}
{{- $rfc3339 := "2006-01-02T15:04:05Z07:00" -}}
+++
title = "{{ .Name }}"
date = {{ now }}
draft = false

orgs = ["{{.Org}}"]
tags = [{{range $i, $tag := .Tags}}{{if $i}}, {{end}}"{{ . }}"{{end}}]
visibilities = [{{if .Public}}"public"{{else}}"private"{{end}}]
terms = ["{{.Term}}"]

# prefix to avoid name collisions
eventId = {{.Id}}
eventStart = {{.Start.Format $rfc3339}}
eventEnd = {{.End.Format $rfc3339}}
eventPublic = {{if .Public}}true{{else}}false{{end}}
eventTerm = {{.Term}}
+++
<!-- generated by lyonscal CLI on {{ now }}. DO NOT EDIT. -->

{{.Desc}}
{{end}}
