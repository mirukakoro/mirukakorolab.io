+++
title = "加速度検知器を用いて3Dモデルを制御してみた"
date = 2021-08-13T13:25:53-04:00
draft = true
tags = ["crafts"]
links = {"コードとサンプルデータ" = "https://gitlab.com/-/snippets/2161795"}
summary = "暇だったので、否、やらないといけない事は一杯~~在った~~在るけれど、惨い現実から逃げたかったので、在ったLSM303を用いた加速度検知器を用いて向きを検出し、3Dモデルを操作する玩具を作ってみました。"
+++

<video controls>
    <source src="video.webm" type="video/webm">
    <source src="video.mp4" type="video/mp4">
</video>

暇だったので、否、やらないといけない事は一杯~~在った~~在るけれど、惨い現実から逃げたかったので、在った加速度検知器を用いて向きを検出し、
3Dモデルを操作する玩具を作ってみました。

## センサ

加速度検知器は前買ったAdafruitのLSM303[^lsm303]を用いたブレイクアウト基盤[^sensor]を使いました。
I²C[^i2c]でセンサ[^sensor]とマイコン[^mikon]を繋いで試してみると、
何故かプルアップ抵抗が必要だとライブラリ[^lib]に怒られました。プルアップ抵抗は不要な筈[^no-pullup]なのに。「何故?」
結局何故か解りませんでした。取り敢えず、4.7kΩのプルアップ抵抗を2つ(`SDA`と`SCL`に)付けることにしました。
[^i2c]: https://ja.wikipedia.org/wiki/I2C
[^lsm303]: LSM303のデータシート {{< src "https://www.st.com/resource/en/datasheet/DM00027543.pdf" 
"http://web.archive.org/web/20210118045919/https://www.st.com/resource/en/datasheet/DM00027543.pdf" >}}
[^sensor]: {{< adafruit "1120" >}}
[^mikon]: {{< adafruit "3857" >}}
[^lib]: [`lsm303_accel`](https://github.com/adafruit/Adafruit_CircuitPython_LSM303_Accel/tree/5d9144d404d8deaab8ec3a0e94063437a4036f08)
[`lsm303dlh_mag`](https://github.com/adafruit/Adafruit_CircuitPython_LSM303DLH_Mag/tree/ddcd47ce2af045f7cf0bf81f7db1d6c3e8c408df)
[^no-pullup]: AdafruitのマイコンのI²Cピンとプルアップについての注意書き {{< src
"https://learn.adafruit.com/circuitpython-essentials/circuitpython-i2c#alert-content-2985156"
"http://web.archive.org/web/20210116004756/https://learn.adafruit.com/circuitpython-essentials/circuitpython-i2c#alert-content-2985156" >}}

マイコンのコード(`code.py`[^code])は0.05秒程に1回`data [heading, point, rotation]`の形式で、
データがシリアルポート(此の場合は`/dev/ttyACM0`)を伝って来るようにしました。(例:`data [45.0, 0.0, -1.11456]`)
此のデータを3Dモデルを制御するPythonプログラム(`main.py`[^code])で読んで、3Dモデルの向きを変える様にしてみたいと思います。

[^code]: [コードとサンプルデータ](https://gitlab.com/-/snippets/2161795)

## 3Dモデルの制御

マイコンからのデータは`bash`等でパイプして、3Dモデルのプログラムの送るので、標準入力を使いました。
此の遊びはラグ等は気にしなくても良いのですが、標準入力を使うと色々[^stdin_1][^stdin_2]遅く成りやすいので、
`async`等をでWebSocket[^websocket]等を使い通信するのが1番[^ws_idea]だと思います。

[^stdin_1]: `input()`や`sys.stdin`はブロックするので、スレッドを使いにくい環境(Panda3Dの`Task`内等)では好ましくありません。
[^stdin_2]: `bash`等がパイプをバッファする可能性が在り、データの伝達が遅くなる場合が在ります。
[^websocket]: 双方向チャンネル通信プロトコル [ウィキペディア](https://ja.wikipedia.org/wiki/WebSocket)
[^ws_idea]: ESP32等を使い、直接WebSocketを使用するか、別プログラムでシリアルポートからの情報をWebSocketで送る等を考えていました。

Panda3D[^panda3d]で簡単な3Dモデルを読み込み、表示するプログラムを書きました。

[^panda3d]: Python用C++ゲームエンジン [公式ウェブサイト](https://www.panda3d.org/)
