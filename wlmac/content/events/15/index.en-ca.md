+++
title = "PA Day"
date = 2021-09-22T21:40:46-04:00
draft = false

orgs = ["School"]
tags = []
visibilities = ["public"]
terms = ["1"]

# prefix to avoid name collisions
eventId = 20
eventStart = 2021-11-19T00:00:00-05:00
eventEnd = 2021-11-19T23:59:59-05:00
eventPublic = true
eventTerm = 1
+++
<!-- generated by lyonscal CLI on 2021-09-22T21:40:46-04:00. DO NOT EDIT. -->


